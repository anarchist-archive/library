<body>
  <heading level="1">
    <title>Thesis 1</title>
  </heading>
  <p>We define the metropolis as the compact group of territories and heterogeneous devices crossed in every point by a disjunctive synthesis; there is not any point of the metropolis, in fact, where command and resistance, dominion and sabotage are not present at the same time. An antagonistic process between two parts, whose relation consists in enmity, totally innervates the metropolis. On one side, it consists, true to it’s etymology, in the exercising of a command that is irradiated on all the other territories – so everywhere is of the metropolis.<fn id="1"/> It is the space in which and from which the intensity and the concentration of devices of oppression, exploitation and dominion express themselves in their maximum degree and extension. In the metropolis, the city and the country, modernity and second natures collapse and end. In the metropolis where industry, communication and spectacle make a productive whole, the government’s required job consists in connecting and controlling the social cooperation which is at the base to then be able to extract surplus value using biopolitical instruments. On the other side, it is a whole of the territories in which a heterogeneous mix of subversive forces – singular, Common, collective – are able to express the tendentiously more organized and horizontal level of antagonism against command. There are not places and non- places in the metropolis: there are territories occupied militarily by the imperial forces, territories controlled by biopower and territories that enter into resistance. Sometimes, very often, these three types of territories cross one another, other times the latter separates itself from the other two and, in yet other occasions, the last enters into war against the first two. The Banlieue is emblematic of this “third” territory: but if everywhere is of the metropolis, then its also true that everywhere is of the Banlieue.<fn id="2"/> In the metropolitan extension of Common life, the intensity of the revolutionary imagination of communism-to-come lives.</p>
  <footnote id="1">
    <p>In the original Italian text “della metropoli” here plays on what would usually be “nella metropoli” or literally “in the metropolis”. Taking an alternate approach, the sense could also be rendered in English using “belongs to the metropolis”.</p>
  </footnote>
  <footnote id="2">
    <p>In reference to the minority dense suburbs of Paris, where over the last few years numerous volatile situations have systematically erupted.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 2</title>
  </heading>
  <p>In the metropolitan struggles, the biopolitical strike defines the principle articulation of the attack strategy that the irreconciliated forms-of-life take against the metropolis of command. Today, the refusal of work cannot be other than the refusal to concede pieces of life, fragments of affections and shreds of knowledge to cybernetic capitalism. Today, struggle against capitalism is the direct removal of bodies from exploitation and attacking revenue, guerrilla warfare against gentrification and violent appropriation of the Common, sabotage of the control devices and destabilization of political and social representation. Likewise, and just as direct, is the wild experimentation in the forms-of-life, liberation of affections, construction of communities, inoculation of happiness and dynamic expansion of desires. Just as bodies – in as much singularity as in population – are the target of the biopolitical police and exploitation, it is only starting from the singularity of bodies that every human, biopolitical, general strike against the metropolis starts: it is in the singularity as form-of-life that holds the Ungovernability that resists biopower.</p>
  <p>Capitalist initiative can be anticipated, at least if diffused singular refusal is accompanied by the decision to build a metropolitan organization of autonomous groups able to bring the rebel forms-of-life to become an insurgent multitude. When singularities rise up as a Common body, the Ungovernable can become revolutionary process.</p>
  <heading level="1">
    <title>Thesis 3</title>
  </heading>
  <p>The blocking tactic is essential to the effectiveness of the biopolitical strike when it is seriously done in the metropolis, which is to say when it exceeds specificity and extends everywhere as a paralysis of control, a circulation block, a counterbehavioral virus, a suspension of production and reproduction, an interruption of the communication factory. In other words: impeding the normal course of capitalist valorization. Through blocks it is possible to recognize the generalized nature of the biopolitical strike. The piqueteros of Buenos Aires<fn id="3"/> and the insurgence against the CPE in France<fn id="4"/>highlighted the force and the capacity of organization. Blocks are material signs of the secession of capital and biopower. Every metropolitan block opens other roads, other passages, other lives: the metropolitan block is necessary for the construction and the defense of the exodus.</p>
  <footnote id="3">
    <p>The piquiteros movement was an important factor in the post-economic collapse of Argentina in 2001. The english picket line was adopted but with an additional emphasis on the impermeability of the block.</p>
  </footnote>
  <footnote id="4">
    <p>idem</p>
  </footnote>
  <heading level="1">
    <title>Thesis 4</title>
  </heading>
  <p>Sabotage responds to the necessity of unifying government destabilization to command deconstruction and thus reinforces the metropolitan blocks. It intervenes on different levels in metropolitan life: from the anonymous singularity that slows the rhythm of value production-circulation to the punctual and devastating intervention of a declared conflict. In the first case, it is a spontaneous, diffused, anti-work behavior, in the second it is subversive intelligence that diagonally interrupts conflict mediation in the governmentability. The subversive science of the metropolis is therefore also defined as the science of sabotage.</p>
  <heading level="1">
    <title>Thesis 5</title>
  </heading>
  <p>When the biopolitical strike, sabotage and blocking converge the presuppositions for metropolitan revolt are created between them. Metropolitan insurrection becomes possible when the chaining together of specific struggles and the accumulation of revolts make a comprehensive strategy that hits (or overtakes) territories, existences, machines and devices.</p>
  <heading level="1">
    <title>Thesis 6</title>
  </heading>
  <p>Social centers,<fn id="5"/> liberated spaces, houses and communized territories, should be to the political critique of the multitudes and transformed into new Mutual Aid Societies. Just as between the 18th and 19th centuries, these territorial aggregations could provide not only solidarity between individuals, mutuality between forms-of-life and organization for both specific and general struggles, but also to the singularity’s and the community’s texture of conscience in that they are both oppressed and exploited. The Common, as a political act, is therefore born as a process in which the friendship and mutuality between those who are deprived transforms itself into a resistance commune. Today, every socialized space can become that place in which an autonomous organization in and against the metropolis is condensed from their rebellious intensity. Temps, workers, gays, students, women, lesbians, teachers, immigrants, queers, children – everyday singularities must be able to refer to these spaces to create revolutionary forms-of-life and organize themselves in so that they are unassailable by the biopolitical police. Common elements – like mutual aid funds, minor knowledges, shared housing, community gardens and parks, autonomous production and reproduction tools, passions and affections – should be salvaged, invented, built, and be available to all those who decide to enter into resistance, on strike, or in revolt. The sum of all of these elements will compose, territory by territory, the Commons of the 21st century.</p>
  <footnote id="5">
    <p>In Italian, “centro sociale” specifically refers to type of squat, or occupied abandoned spaces that are converted into self-run collective projects. There are as many variations as there are examples throughout Italian territory, including concert halls, libraries, restaurants, pubs, etc..</p>
  </footnote>
  <heading level="1">
    <title>Thesis 7</title>
  </heading>
  <p>The only security to which non submissive forms-of-life aspire is the end of oppression and exploitation. The material and ethical poverty that the biopower constrains millions of men and women to is the source of the insecurity that reigns in the metropolis and governs over the population. Against this, we can’t fall into the loophole of asking for rights, which means more government and therefore non-liberty: the only Common law is created and determined through its revolutionary exercise. Every desire, every need that the forms-of-life of the multitudes are able to express are in their right. In doing so, they lay the law.<fn id="6"/></p>
  <footnote id="6">
    <p>The Italian “diritto” has the double meaning of both “right” (as in a civil right) and “law”. Obviously, law here is not intended to mean some legal procedure but what could be called a Common right.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 8</title>
  </heading>
  <p>Without rupture there is no possibility of bringing the escape routes beyond command. Every rupture corresponds to a declaration of war by the rebel forms-of-life against the metropolitan Empire: remember Genoa 2001.<fn id="7"/> In the metropolis, an asymmetry between biopower and forms-of- life rules, but it is exactly this asymmetry that can become a fundamental weapon in metropolitan guerrilla warfare: the impact between forms-of-life and command creates an excess and, when it is expressed with force and strength, can become revolutionary organization of Common life.</p>
  <footnote id="7">
    <p>The mobilizations against the G8 summit of 2001 in Genoa.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 9</title>
  </heading>
  <p>In the metropolis, the articulation and the linking of different forces and not mediation is what pushes their intensity to drive the game of subversive alliances. The construction and the effectuation of the Rostock revolt, against 2007’s G8, showed the potency of this “game<fn id="8"/>.” Autonomy, as a strategic indication for the succession from biopower, means the political metropolitan composition of all of the becoming-minor into a becoming-Common, a horizontal proliferation of counter-behaviors dislocated on a single plane of consistence without ever producing a transcendent unit. In the metropolis there is no revolutionary Subject: there is a plane of consistence of subversion that brings each singularity to choose it’s part.</p>
  <footnote id="8">
    <p>The Rostock demonstrations were characterized by a veritable mixing of the plurality of variated groups, and the adopting a much more fluid form in respect the usual “bloc” formations. The result was a colorful mass of different tactical expressions that was extremely difficult for the law enforcement bodies to counter. Excess, in all of its forms, is the expression a struggle’s truth. What remains after every struggle is always a Common truth.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 10</title>
  </heading>
  <p>he important part of every social metropolitan movement is found in the excess which it produces.</p>
  <heading level="1">
    <title>Thesis 11</title>
  </heading>
  <p>Without a shared language, there is never any possibility of sharing any sort of wealth. Common language is constructed only in and by struggles.</p>
  <heading level="1">
    <title>Thesis 12</title>
  </heading>
  <p>One of biggest dangers for the autonomous forms-of-life is indulgence in the technical separation between life and politics, between managing the existent and subversion, between goods and Common use, between enunciation and material truth, between ethics and blind activism for its own sake. The confusion between what is Common and what is held in property, in individualism and in cynicism, should be defeated in practice, which is to say through an ethic of the Common forged in conflict.</p>
  <p>The personal is biopolitical, politics are impersonal.</p>
  <heading level="1">
    <title>Thesis 13</title>
  </heading>
  <p>The metropolitan architectures of autonomy are all horizontal. As such, they adhere to the form-of- organization in all of their constitutive political stances and vice versa. Those of power, in every form and everywhere it is present, are all vertical and that is how they separate individuals from the Common. These architectures are to be deserted, surrounded, neutralized and, when it is possible, attacked and destroyed. The only possible hierarchy in metropolitan autonomy is in the clash with dominion.</p>
  <heading level="1">
    <title>Thesis 14</title>
  </heading>
  <p>The form-of-organization, in the present historical conditions, cannot be other than the form-of-life. It is non-normative regulation of the Common for the Common. Here discipline does not mean other than the Common organization of indiscipline. The form-of-organization is the plane of consistency on which individuals and multitudes, affections and perceptions, reproduction tools and desires, gangs of friends and indocile artists, arms and knowledge, loves and sadnesses circulate: a multitude of fluxes that enter in a political composition that permits everyone’s power to grow while, at the same time, diminishes that of the adversary.</p>
  <heading level="1">
    <title>Thesis 15</title>
  </heading>
  <p>In the metropolis, individuals are only the bodily reflection of biopower, whereas singularities are the only living presences capable of becoming. Singularities love and hate while individuals are unable to live these passions if not through the mediation of the spectacle in such a way that they are governed an neutralized even before being able to arrive to the presence. The individual is the base unit for biopower whereas the singularity is the minimum unit from which every practice of liberty can begin. The individual is the enemy of the singularity. The singularity is the most Common we can be.</p>
  <heading level="1">
    <title>Thesis 16</title>
  </heading>
  <p>The moment has come to put the category of “citizenship”, the heredity of an urban modernity that doesn’t exist in anywhere, into discussion. In the metropolis, being a citizen means simply reentering in the biopolitical job of governmentability, seconding the “legality” of a State, of a Nation and of a Republic that doesn’t exist if not only as ganglion of the Empire’s organized repression. The singularity exceeds citizenship. Vindicating one’s own singularity against citizenship is the slogan that, for example, migrants write daily with their blood on the Mediterranean coasts, in the CPT in revolt,<fn id="9"/> on the wall of steel that divides Tijuana from San Diego or on the membrane of flesh and cement that separates the Rom bidonvilles<fn id="10"/> from the shamefully sparkling City Center. Citizenship has become the award for faithful allegiance to the imperial order. The singularity, as soon as it can, happily does without it. Only the singularity can destroy the walls, borders, membranes and limits constructed as the infrastructure of dominion by biopower.</p>
  <footnote id="9">
    <p>Centro di Permanenza Temporanea” litteraly translated would be “Temporary stay center” which is quite misleading: CPT are prison structures used to hold people caught without stay permits, usually destined for deportation.</p>
  </footnote>
  <footnote id="10">
    <p>A bidonville is a small area, usually in abandoned areas of a city, where a migrant Rom population lives, quite similar to migrant camps found in the US.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 17</title>
  </heading>
  <p>Just as capitalist revenue parasitically exploits metropolitan social cooperation, politics coincides with the parasitic revenue of the government on the multitude’s forms-of-life: violent or “democratic” extortion of consensus, the privately public use of the Common, and the abusive exercise of an empty sovereignty over society are the ways that political revenue fattens itself in the shade of the global capital skyscrapers. In the metropolis, only the political remains as a possibility of exercising the Common and multitudinarian deadline for its appropriation. One should never do some politics, if to reach the “point of no return”. Politics are always a form of government. The political is, sometimes, revolutionary.</p>
  <heading level="1">
    <title>Thesis 18</title>
  </heading>
  <p>The biopolitical metropolis is administrated exclusively using governance. Social movements, autonomous forces and all those who truly have the desire to subvert the status quo understand that when a struggle begins one should never commit the fatal error of going straight to negotiate with governace, sit at it’s “tables”, accept its forms of corruption and thus become its hostage. On the contrary, it is necessary right from the beginning to impose the battleground, the deadlines and even the modality of struggle on governance. Only when the balance of power is overturned in favor of the metropolitan autonomy will it be possible to negotiate governance’s surrender while standing up, on solid legs. The extraordinary insurgence of Copenhagen<fn id="11"/> demonstrates that which is possible, if only one has the courage to take the initiative and persevere as oneself.</p>
  <footnote id="11">
    <p>A reference to the campaign of resistance to the eviction of the Ungerdomshuset collective house in Copenhagen.</p>
  </footnote>
  <heading level="1">
    <title>Thesis 19</title>
  </heading>
  <p>In the metropolis, just as work has become superfluous, paradoxically, everyone has to work all the time, intensively, from the cradle to the grave and maybe beyond; evidently the compulsion to work is evermore obviously a political obligation inflicted upon the population so they will be docile and obedient, serially productive of goods and individually occupied in the production in and of themselves as imperial subjects. We vindicate the refusal of work and the creation of other forms of production and reproduction of life that are not burdened under salary’s yoke, that are not even linguistically definable by capital, that start and finish with and in the Common. Guaranteed metropolitan income can become a Common fact only when the practices of appropriation and the extension of autonomy over the territory massively impose a new balance of power. Until that moment, it’s probable that it will instead be – as, for example, what happens in the local and regional proposals of a so called “citizenship income” – another passage in the fragmentation of the Common and in the hierarchy of the forms-of-life. Moreover, as the autonomous experiences of the ’60s and ’70s have taught us, it is only when we are effectively capable of putting our very lives in Common, of risking them in the struggle, that any egalitarian vindication has sense. In our history, there has never been an economic vindication that wasn’t immediately political: if factory workers said “more salary for all” to mean “more power to all”, today “income for all” means “power shared by all”. As singularities that have chosen to be on the subversive side, we must have the courage to construct and share the Common above all among ourselves. This is what will make us strong.</p>
  <heading level="1">
    <title>Thesis 20</title>
  </heading>
  <p>A new sentimental education is in course in the rebel communities, it’s invention and it’s microphysical experimentation is on the agenda of every true revolutionary experience that fights against the Empire today. One cannot speak of friendship, of love, of brotherhood and sisterhood, if not as a part inside the strategic advancement of the insurrection against biopower and for the Common. In the same moment in which a friendship comes to exist, that a love becomes a force of the Common, or a gang constitutes itself to fight dominion, their enemy appears on the horizon. The destruction of the capitalist metropolis can only be the fruit of an irreducible love, of the Common effort of all the singularities that will rise up with joy against the priests of suffering and the hired thugs posted to defend the Towers of command. The communism-that-comes will be generated by the forms-of-life of the multitudes that will have chosen the party of the Common against biopower.</p>
  <break type="dinkus"/>
  <p><strong>Make Plans. Be Ready.</strong></p>
</body>
