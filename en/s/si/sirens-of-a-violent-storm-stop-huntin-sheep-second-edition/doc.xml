<body>
  <heading level="1">
    <title>Definition of Terms</title>
  </heading>
  <p><strong>Informant:</strong> <em>a person recruited by police to provide information</em></p>
  <ul>
    <li>Is a member(s), friend(s), or associate(s) of group</li>
    <li>Referred to as 'Confidential Source' or 'Confidential Informant' by police</li>
  </ul>
  <p><strong>Infiltrator:</strong> <em>A person who infiltrates a group by posing as a genuine member.</em></p>
  <ul>
    <li>May be military, police, intelligence, corporate, private contractor, 'patriot'</li>
    <li>May be citizen facing imprisonment</li>
  </ul>
  <p><strong>Snitch:</strong> <em>Someone who gives up incriminating evidence to authorities.</em></p>
  <p><strong>Snitch Jacket:</strong> <em>Reputation for being an informant. It is used both in police jargon and street slang. Jacket comes from the "file jackets" that were used by the police prior to computerization of records. The phrase has part of its origins in the police interrogation tactic of threatening criminals who will not cooperate. Ironically police officers have been known to threaten to publicize or have correctional officers publicize that a perpetrator's "jacket" says they are an informant to get them to inform.</em></p>
  <p><strong>Network:</strong> <em>A social structure made up of individuals (or organizations) called "nodes", which are linked (connected) by one or more specific types of interdependency. Radical Networks may have complex links based on friendship, sharing living space, common interest, common organizational practice, membership in organizations, shared identity, sexual relationships and connections to a physical space.</em></p>
  <heading level="2">
    <title>5 Basic Infiltrator Types</title>
  </heading>
  <ol>
    <li><strong>Hang Around:</strong> less active, attends meetings, events, collects documents, observes &amp; listens</li> <li><strong>Sleeper:</strong> low-key at first, more active later</li>
    <li><strong>Novice:</strong> low political analysis, 'helper', builds trust and credibility over longer term</li>
    <li><strong>Super Activist:</strong> out of nowhere, now everywhere. Joins multiple groups or committees, organizer</li>
    <li>
      <p><strong>Ultra-Militant:</strong> advocates militant actions &amp; conflict</p>
      <ul>
        <li><em>Agent Provocateur: incites illegal acts for arrests or to discredit a group or movement</em></li>
      </ul>
    </li>
  </ol>
  <p><strong>Light Undercover:</strong> <em>may have fake ID, more likely to return to family life on weekends, etc.</em></p>
  <p><strong>Deep Undercover:</strong> <em>fake gov't-issued ID, employment &amp; renting history, etc.</em></p>
  <ul>
    <li>May have job, apartment, partner, or even family as part of undercover role</li>
    <li>Lives role 24-hours day for extended time (with periodic breaks)</li>
  </ul>
  <heading level="1">
    <title><strong>Part 1:</strong> An Introduction</title>
  </heading>
  <epigraph>
    <content>
      <p>GOD: I own you like I own the caves.
      <br/>THE OCEAN: Not a chance. No comparison.
      <br/>GOD: I made you. I could tame you.
      <br/>THE OCEAN: At one time, maybe. But not now.
      <br/>GOD: I will come to you, freeze you, break you.
      <br/>THE OCEAN: I will spread myself like wings. I am a billion tiny feathers. You have no idea what’s happened to me.</p>
    </content>
    <source><p>Dave Eggers</p></source>
  </epigraph>
  <p>It must be made clear that if there is one thing to take from this pamphlet, there are no fool proof methods for routing out undercover’s and informants. This pamphlet is about exploring possibilities for countering covert investigative efforts initiated or assisted by police. The objective of countering all aspects of state-led intelligence gathering is not inherently to reveal undercover activity but to create a safer and less penetrable network to operate out of. Dialogue about this issue need to be addressed with a bit of finesse as there are many dangers, disservices and fruitless avenues people worried about undercover investigative operations can explore. It is clear that our practices in dealing with undercover investigations need invigorated theoretical and practical attention in a manner that we can communicate across our personal networks. In the last several years undercover operatives have been suspected or confirmed in radical networks across the country. In the courtrooms, holding cells and on the gallows, or navigating new worlds free from imposition and misery, we will realize it is only us who can organize our own safety and only our choices that can prepare us for freedom.</p>
  <p>There appears to be a rise in known infiltration investigations in North American radical networks, with thorough destabilizing effects on our capacities to struggle, comrades facing heavy repression and of course, the less obvious consequences on our personal mental states. The place that we start is with dialogue. We realize that organizing in radical environments has led many of us to have experiences already with undercover operatives. We have all critically thought about dealing with them, and had personal experience or have heard historical stories of individuals and networks that have dealt with them in the past. We all come from unique organizing environments, and both our networks and police investigative operations are incredibly dynamic. The need for dialogue and personal reflection on methods to provide greater protection for ourselves and the networks we organize out of has become an unavoidable dilemma to confront. Our analysis of the shifting terrain that makes our networks grow and disband, and thorough communication of these understandings to other radical networks are our strongest tools for subverting covert police operations.</p>
  <p>A pamphlet that deals with addressing ways to combat undercover
investigative work needs to explain the role of an undercover in relation to much broader investigative efforts of police. I.e. undercover’s and informants do not exist in vacuums. They are not lone gunmen vigilante types. They are employed in specific investigations to gather information, build cases against people and possibly destabilize the effectiveness of a network. If there is an undercover operative in your network, they are a visible manifestation of a larger investigation which often but not always includes surveillance operations, groomers and handlers, and people working on the more technical aspects of information gathering. In the case of a recent undercover police operation, it has been revealed that the undercover was always in very close proximity to two other police officers, while in the presence of people in the radical networks they were embedded in. They also had a handler who they met with morning and night to review notes and make daily objectives, and there were many more police involved in surveillance operations.</p>
  <p>There are also various types of covert operatives that have infiltrated and destabilized both radical and criminal organizations. Briefly, there are both shallow and deep undercover’s. Informants that range from people embedded deeply in radical movements that decide to switch sides and build cases as well as former allies that role under repressive pressure. These notes only deal with informants and police who are entering networks, not states witnesses and heavily embedded informants who have developed a long history of trust. The question of how to create networks that are uncompromisingly free of snitches, people who cross the line and states witnesses need to be addressed on a more fundamental level in different settings. <strong>For various case studies, research Anna Davies, Jacob Ferguson, William O’Neal, Rob Gilchrist, Dave Hall, Jay “Jaybird” Dobyns, Alex Caine, Brendan Darby, Brenda Dougherty, Khalid Mohammad, Andrew Darst.</strong></p>
  <p>Protecting your safety is protecting everyone’s safety. The goal of anarchist agitation is to build a social force that has the potential to destroy hierarchical institutions and paradigms with solidarity. Other goals include: building infrastructure and autonomous space, to intervene in conflict, to push tensions to conflict, and to realize the potentials and interconnectedness of our personal and collective freedom. Anarchists expose that liberal concepts of individual freedom are predicated on dominance and apathy towards others, whereas individual freedom as an anarchist concept cannot be severed from the collective, but can also only be personally defined. An example of this can be seen in offensive struggles and the relevance of solidarity central to the anti-authoritarian spirit. Attacking police for instance in Vancouver, is a direct act of solidarity with people in Guelph or anywhere else who face the same institutions of repression. Through these attacks, the weakening and the example of insolence has implications on the infallibility of police as enforcers of social morality and our collective ability and agency to fight them and win back decentralized control.</p>
  <p>On a similar level, our ability to organize ourselves in a manner that is effective in staving off the investigative efforts of the criminal justice system, while maintaining a social presence, is interwoven with our concepts of freedom. I have heard people who have just been dealt the devastating effects of undercover police pillaging their social network say, <em>"the lesson to learn is that I need to distance myself from people I am not confident in and work on projects with people I know well."</em> The issue is that if we see undercover operations as a threat to our personal freedom only, we make half efforts that remove ourselves from danger and leave our networks open to attack. If we individually investigate and critically examine all the links in our networks instead of removing ourselves from parts of them, we provide a greater security to our network and ourselves. We are strengthened by the acts of mutual aid and solidarity, they protect us and at the same time make us more dangerous and uncontrollable.</p>
  <p><em>"Let the pigs join our activist group, they can cook our food and wash our dishes. They aren't going to get shit, because I got nothing to hide."</em> It is still a fairly prevalent idea that covert police investigations don't really harm networks if the more clandestine culture within these networks stays well sealed from the outside. I.e sick them on the activist groups or if you are concerned about someone, let them stay involved in a peripheral way as long as they don't get close. The concept comes out of the conceited notion that the militant is the center of investigative efforts. This logic does not consider that criminal investigations into anti-authoritarian networks are meant not just to criminalize militant resistance, but destabilize and undermine the networks themselves and create social profiles.</p>
  <p>The mentality of the <em>laissez-faire</em> anarchist in relation to investigative efforts comes out of laziness, not wanting to upset the herd, not wanting to make yourself look like a person who is concerned about police investigations, not wanting yourself to look like you are snitch jacketing someone, not having the tools to inquire further about someones background, and feeling helpless or isolated and probably other reasons as well. It is human to have these feelings and rationalities but it is ultimately the most dangerous thing to do. In the absence of being routed out of networks, covert operatives end up building credentials through association, building intensive social profiles on everyone, finding pressure points to cause tension and conflict within networks, entrapping people, and monitoring our daily lives from the comfort of our living rooms.</p>
  <p><strong>A final note:</strong> There may be people in your network that you are uncomfortable with or find disruptive to organizing efforts. They may not be an undercover operative but still need to be confronted or removed from an organizing capacity to provide safety or a more functional network. Although the goals may not be the same, the destabilizing effects of these relationships on networks have similar effects and should be openly discussed.</p>
  <heading level="1">
    <title><strong>Part 2:</strong> The Practical Side of a Safer Network</title>
  </heading>
  <epigraph>
    <content><p>Thank you for teaching us that, against power, the only lost battle is the one not fought.</p></content>
    <source><p>Diego Rios</p></source>
  </epigraph>
  <p>We attend discussions, read information on and do research about the history of repression in radical networks at least partially to learn practical lessons that apply to our life. Below is an attempt to develop an incomplete set of guidelines for discussion which can be adapted and applied to our networks today.</p>
  <p>Briefly we have included some broad suggestions for tools that may be helpful in aiding personal efforts to create a stronger base of safety.</p>
  <heading level="2">
    <title>Building Your Toolbox</title>
  </heading>
  <ul>
    <li>Understand and research the different types of risks that are posed from undercovers, informants and state witnesses.</li>
    <li>Research the historical case studies and impacts of undercover’s, informants and snitches on social movements and underworld tendencies.</li>
    <li>Review relevant police literature on investigative techniques, to gain insight into ways undercover police operations may function and to develop investigative techniques to use in combative ways and gain security.</li>
    <li>Review literature and ongoing discussions related to security culture.</li>
    <li>Examine the history of organizing methods used in radical networks, revolutionary organizations in different eras and places and comparing them to modern affinity-based organizational models of today’s anarchist networks</li>
    <li><strong>For historical examples research:</strong> OCAP, Os Cangaceiros, Rote Zora, The A.L.F/E.L.F, The Red Army Faction, The I.R.A., The Black Panthers, Insurrectionary Anarchism, Autonomist movements and Anti-fascist resistance in occupied Europe during WWII. Or read books such as <em>We Are An Image Of The Future</em>, <em>The Subversion Of Politics</em>, <em>Agents of Repression: The FBI’ Secret War Against the Black Panther Party and the American Indian Movement</em>, <em>Black Mask &amp; Up Against The Wall Motherfucker</em>, <em>Argentina’s Anarchist Past: Paradoxes of Utopia</em>, <em>Confronting fascism: Notes On a Militant Movement Direct Action</em>… etc.</li>
  </ul>
  <heading level="2">
    <title>Security Guidelines for Discussion</title>
  </heading>
  <epigraph>
    <content><p>It is easy to hit a bird flying in a straight line.</p></content>
    <source><p>B. Gracian</p></source>
  </epigraph>
  <p>This is a security guideline for developing safer networks into 6 parts for further discussion. There will never be single solutions. This model may provide suggestions that guide a more secure practice. Ultimately, these structured ways of creating more secure networks must be very dynamic to continue relevance. As investigative efforts adapt, so do our practices to stay ahead.</p>
  <p><strong>They are:</strong></p>
  <ol>
    <li>Creating a base of safety list</li>
    <li>Creating a network map</li>
    <li>Tactics for further inquiry</li>
    <li>Communicating with your base</li>
    <li>Communicating with a potential police informant</li>
    <li>Concluding action</li>
  </ol>
  <heading level="2">
    <title>Create a Base Safety List</title>
  </heading>
  <p>Create a list of people that are involved in your networks.</p>
  <p>Asking yourself a series of structured questions which reveal your level of safety with an individual in the network.</p>
  <ul>
    <li><em>who are the people close to you?</em></li>
    <li><em>how do you know them?</em></li>
    <li><em>who are your comrades (people you work on projects with)?</em></li>
    <li><em>who are the people you likely enter confrontation with?</em></li>
    <li><em>what is their historical connection to you?</em></li>
    <li><em>how did you meet, where did you meet?</em></li>
    <li><em>through which people were you introduced?</em></li>
    <li><em>have you met their other friends from different social networks?</em></li>
    <li><em>have you met their families?</em></li>
    <li><em>can people you trust verify their history?</em></li>
    <li><em>are there aspects of their life you have a hard time communicating about or verifying (work, home, vehicle, aspects of their past)?</em></li>
    <li><em>have you clearly talked about and are satisfied with the intentions of the people you organize with on the projects you mutually work on?</em></li>
    <li><em>do you like how they communicate to others about similar experiences you have had with them?</em></li>
    <li><em>do you have a strong sense of trust? why?</em></li>
  </ul>
  <p>You will now have divided lists of people. Some of which you were at ease answering the above questions for and feel very secure and trusting with: This is your base of safety. Other people on the list you may know varying degrees of information about but have revealed that aspects of their life or the way you relate to them may be aloof to you. You want to communicate more with them before adding them to your base of safety. You will realize that a hierarchy of knowledge and safety will probably develop, where some people may just need small conversations to feel more secure with, and other people may need a lot of effort to reveal safety.</p>
  <p>On a personal level investigative lists like these are formal extensions of our choices in association we make mentally on a daily basis. This exercise is to sharpen our ability to make informed and critical choices about the people we associate with. The goals in these assessment questions are to critically understand the social relations that make up day-to-day interactions with the broader network you commonly relate to. Analyzing relationships in this manner maybe effective in both mapping and realizing a network of relative safety, while exposing aspects of people you want to learn more about in the hopes of them becoming safer links in your network. The use of exercises like this affirms a base of safety and allows for pro-active individual research, preferably in periods of relative calm. Taking the time and energy to do this work are steps towards critical and empowering choices related to our safety that steal agency from the grips of paranoid haplessness and fear.</p>
  <heading level="2">
    <title>Create a Network Map</title>
  </heading>
  <p>Place the list of people in your network on to a network map. Use 3 different color pens or markers to write peoples names on the map, depending on whether they are on your new base of safety, or someone you would like to know more about before adding them to your list.</p>
  <p><em>Colour 1) Base Of Safety</em></p>
  <p><em>Colour 2) People that need slightly more communication with.</em></p>
  <p><em>Colour 3) People that require a lot of communication.</em></p>
  <p>Now create links using more colors to reveal the perceived connections of people within the network.</p>
  <p><em>Colour 4) Who lives together</em></p>
  <p><em>Colour 5) Who are people closest to you in the network</em></p>
  <p><em>Colour 6-?) Use markers to define project membership to the best of your ability. I.e 1 marker will be used to connect the members of your local Food Not Bombs group, while another marker will be used to define the Books to Prisoners group.</em></p>
  <p><em><strong>Note: It would be foolish to include clandestine organizational efforts in this list.</strong></em></p>
  <p><strong>Your completed map will now reveal several details:</strong></p>
  <ul>
    <li>The level at which people are embedded in your networks by the amount and types of links they have.</li>
    <li>The types of social connections that people have to each other in a network.</li>
  </ul>
  <p><strong>It could reveal…</strong></p>
  <ul>
    <li>That someone you are interested in more communication with is also close to people that are on your base of safety.</li>
    <li>There are people you or other people in your base of safety organize with that have tenuous social connections.</li>
    <li>Do you need help from people in your base of safety to assist in the inquiry?</li>
  </ul>
  <heading level="2">
    <title>Tactics for Further Inquiry</title>
  </heading>
  <p>It is imperative to see the people you want to know more about as people with the potential to be in your safety network. If you believe that there is no way you will ever feel safe with that person in your network, there are probably more issues than just untrustworthy behavior. Consider talking with very close friends from your base of safety about options, such as, removing that person from your network, or having a discussion with the person around why you do not want to organize with them.</p>
  <heading level="3">
    <title>Soft Questions</title>
  </heading>
  <p>Soft questions are meant to be asked in subtle and undetected ways and are aimed at revealing information in a way that masks intention of the questioner.</p>
  <p>Think about the environment and atmosphere and attempt to control the environmental variables for the questions. A relaxed and comfortable person is more likely going to have their guard down. They are more likely going to indulge you to keep up the pleasantries of conversation. It is also impossible to detect shifts in body language and facial expression when people are stressed out. Subtle and benign questions focused around the direction of aspects of their life that you would like to know more about may help. If you want to understand their past better, for example, during a friendly conversation you could steer the direction of conversation to your family history, and maybe ask questions like: What is your mom’s name? Did she keep her maiden name or is that your dads last name too?</p>
  <heading level="3">
    <title>Hard Questions</title>
  </heading>
  <p>Hard questions are meant to be interrogative. They are meant to put the person you are communicating with on edge, to let them know that you are serious about retaining information.</p>
  <p>These types of questions are aimed at revealing information through implied coercion. They work with questions that you can verify in the moment. Where were you born? Where did you go to primary school? What is your birthday? What is your middle name? What job do you have? Give me your parents phone number and wait here with me while I verify the information…</p>
  <heading level="3">
    <title>Physical Surveillance</title>
  </heading>
  <ul>
    <li>License plates and VIN numbers</li>
    <li><p>Addresses for surveillance (garbage checks, visits)</p>
      <ul>
        <li>Refer to Tracking and Monitoring Supplement</li>
      </ul>
    </li>
  </ul>
  <heading level="2">
    <title>Case Studies</title>
  </heading>
  <p>On the East Coast a freedom of Information request led to the deduction of an operational informant, and through investigative efforts they narrowed their search and surveilled a potential informant until confirmation.</p>
  <p>In Pittsburgh during the lead up to the G-20 a pop questionnaire was put on everybody that attended a meeting. When one person could not answer the questions adequately, they were asked to leave the meeting and disappeared from the network.</p>
  <p>When traveling to some networks in Europe it is common for people to ask you for background checks involving phone numbers of people close to you and other verifying information before you enter the network.</p>
  <p>A license plate check through the Ministry of Transportation in Ontario may reveal who the owner of a car is, and whether the car is a fleet vehicle or belongs to a company that deals with law enforcement.</p>
  <p>Research in Guelph related to verification of Brenda Dougherty as a student at the local university, could have outed her as an undercover as early as September 2009.</p>
  <heading level="2">
    <title>Communicating With Your Base</title>
  </heading>
  <p align="center"><em><strong>“I think shes a cop.” “Why?” “Did you see the clothes she was wearing, and she asked me what I thought about how the demo went.” “Are you wasted!”</strong></em></p>
  <p>Contrary to the very common, very uninformed snitch-jacketing that goes on in anti-authoritarian networks, we need to develop a security model that limits paranoia through gathering intelligence and communicating in ways that refrain from alarm and sensationalism.</p>
  <p>All communication approaches are contextual, these suggestions are based on personal experience and reflection and may not apply.</p>
  <p>The importance and delicacy of communication with your network can not be understated. Security issues have a way of bringing out irrational, frustrated and upsetting tendencies within most people. It is hard to broach a conversation that focuses on the idea that a person you know could potentially be manipulating and deceiving you for malicious purposes and in many ways can cause strong tension and divisions amongst the network.</p>
  <p>In my experience with conversations related to dealing with potential undercovers, there was always a strong sense of division and frustration amongst close friends on how to approach the person, if at all. With this knowledge, think about ways to disarm and de-escalate potentially divisive conversations with people before you have them. The place to start communication is on the ground floor of general inquiry with explanations that build cases for more research on an individual or add people to a position on your base of safety.</p>
  <p>Think hard about how you want to reveal information you have to your very closest comrades, to people who are closest with people you are inquiring about, and of course to the individual you are interested in with the goal being a zero tolerance for gossip and hurtful rumors. The objective of good communication as is the objective of countering all aspects of State-led intelligence gathering is not inherently to reveal undercover activity but to create a safer and less penetrable network. This desire for personal and collective safety can be helpful in communication with hostile people in the network over the desire to find a rat that may not exist.</p>
  <heading level="2">
    <title>Communicating with Potential Undercover Operatives</title>
  </heading>
  <ul>
    <li>Know that if they are in your presence and they are working, they very well may not be alone, in terms of recording devices or unseen law enforcement.</li>
    <li>Wait for confirmation before allegations.</li>
    <li>Watch the ways you threaten people and make choices based on well thought out plans. Intimidating a peace/police officer is becoming a more widely used charge.</li>
    <li>Not revealing intentions and a friendly attitude can be more appropriate for gleaning information than interrogative communication.</li>
  </ul>
  <heading level="2">
    <title>Concluding Action if Undercover Informants Discovered</title>
  </heading>
</body>
