<body>
  <p><em>“If voting changed anything, they’d make it illegal.” – Emma Goldman</em></p>
  <p>Roe v. Wade has been overturned, allowing states to make abortion (i.e. any medical procedure that harms the “unborn” regardless of context) a felony. What other rights get taken away when the government decides a voluntary medical procedure that <em>you</em> agree to is now a crime? You can’t vote anymore because you are a criminal. Another choice has been taken away from you.</p>
  <p>Trans men, nonbinary and gender non-confirming folks – all people with wombs are affected by this decision. But abortion has never been the sole issue. This is an assault on the right of every human being to live as we see fit, to do whatever we want to with our own bodies, and to make choices without the threat of discrimination, violence, or incarceration. This decision is so much more than <em>just</em> abortion, <em>just</em> medical autonomy, <em>just</em> the reinforcement of patriarchy: it is the latest iteration of the state’s campaign against all choice, all autonomy, and everyone who is oppressed. Why? To maintain the status quo at all human costs. Thus is the function of the courts.</p>
  <p>President Biden insists we must “keep all protests peaceful,” further claiming that “Violence is never acceptable. Threats and intimidation are not speech.”</p>
  <p>To set the stage of our immediate electoral future, here are some of the people for whom “peaceful protest” will no longer be an option:</p>
  <ul>
    <li>
      <p>Women seeking abortions</p>
    </li>
    <li>
      <p>Men seeking abortions</p>
    </li>
    <li>
      <p>Teenagers of all genders and orientations seeking abortions</p>
    </li>
    <li>
      <p>Parents trying to support their kids</p>
    </li>
    <li>
      <p>Rape victims</p>
    </li>
    <li>
      <p>Victims of domestic abuse</p>
    </li>
    <li>
      <p>People with ectopic pregnancies</p>
    </li>
    <li>
      <p>Medical professionals who provide reproductive care</p>
    </li>
    <li>
      <p>Gestational surrogates</p>
    </li>
    <li>
      <p>Sex educators</p>
    </li>
  </ul>
  <p>Let’s not forget the people for whom peaceful protest was <em>never</em> an option:</p>
  <ul>
    <li>
      <p>Undocumented immigrants</p>
    </li>
    <li>
      <p>The unhoused</p>
    </li>
    <li>
      <p>Refugees</p>
    </li>
    <li>
      <p>Sex workers</p>
    </li>
    <li>
      <p>Disabled people</p>
    </li>
    <li>
      <p>Children, who have forever existed in a world where their bodies don’t belong to them</p>
    </li>
  </ul>
  <p>Millions of people in this country are denied the vote because the state has made their lives illegal, codifying resistance to systems that want them dead as crimes. The vote itself has yet to be made illegal – perhaps a testament to its inefficacy, if Goldman is to be read literally – but we’re now faced with perhaps a worse situation than Red Emma predicted: while the vote remains legal, individuals are being made illegal. The vote has never been a tool available to all of us, and it sure as hell won’t save those of us whose lives depend on acts that forfeit our access to the ballot box.</p>
  <p>“Personally, I’m against the use of state power to impose anyone’s values on others,” a self-described “traditionalist” libertarian might add, “but a life is a life, and I believe life begins at conception.” Let me ask you this: what would you do to defend that life? I’m not asking what it is you want done, I’m specifically asking what would <em>you</em> as an individual do to protect that which you call “life.” Do you debate with pro-choicers in an attempt to convince them of your perspective? Do you show up to a “march for life” rally at risk of being associated with those you insist you disavow? Do you violently interrupt the daily operations of an abortion clinic, disrupting staff in the middle of providing lifesaving care?</p>
  <p>As much as it may feel or look like it, you are not defending anyone – especially not the unborn. An abortion clinic under siege by private citizens, cops, and the highest court in the land is going to provide worse care, make poorer decisions, and ultimately harm the unborn fetuses you want so dearly to protect. This isn’t wild speculation, it’s just how stress works; services provided under duress (legal or otherwise) are generally worse than the same services provided in secure environments. Absent attacks from all sides, further consideration can be given to pressing decisions, resources can be managed with greater efficiency, and, most importantly, those using the service can communicate openly without fear. The unborn, believe it or not, benefit from medical institutions (yes, including abortion clinics) operating smoothly. This is why we’re against state intervention in medicine after all; doctors can’t suggest the best medications and provide the best treatment because governments make certain “bad drugs” illegal, ban “bad procedures,” and strip “bad doctors” of their licenses. Supporting the repeal of Roe v. Wade means supporting regulated medicine, conducted not between doctors and patients (i.e. a voluntary, confidential transaction), but between doctors and a myriad of special interests, external authorities, and the will of a faceless mob who want to reshape society in their image. None of this is in the interest of the unborn, and it’s certainly not a libertarian outcome. Put bluntly, if you are “pro-life,” you have been lied to. You are being used by powerful people to advance a christian nationalist agenda that will ultimately culminate in unmitigated tyranny, signed off by an unstoppable entity legally accountable to no one.</p>
  <p>We, on the other hand, stand for unrestricted freedom, universal liberty, and the complete rejection of governments, corporations, and any individual who claims authority over us – <em>especially</em> in matters so personal as what happens to our own bodies. When the white market denies access to lifesaving medication, we make <a href="https://fourthievesvinegar.org/">our own abortion pills</a>; when the government shuts down free and open communication between patients and doctors, we <a href="https://www.fwhc.org/jane.htm">create secure networks of care</a>; when hormone access is restricted, we <a href="https://www.vice.com/en/article/xw7z4j/hacking-diy-estrogen-hormones-trans-people">teach people how to make their own estrogen</a>; when the state makes a choice illegal, we make sure that choice remains possible for everyone.</p>
  <p>The right of every living being to absolute bodily autonomy is non-negotiable. Any institution that insists otherwise is begging to be unceremoniously abolished. We have all been aggressed upon by that which the state calls “law.” Don’t be surprised if the disaffected among us decide to respond with acts the state labels “crime.” We always have, and we always will.</p>
  <p>Expect resistance.</p>
</body>
